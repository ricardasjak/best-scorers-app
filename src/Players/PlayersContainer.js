import React, {useState, useEffect} from 'react';
import PlayersList from './PlayersList';
import {getBestScorers} from './playersApi';

const initial = [
  {id: 101, player: 'Ronaldo', goals: 144, team: 'Juventus'},
  {id: 303, player: 'Messi', goals: 125, team: 'Barcelona'},
  {id: 909, player: 'Zlatan', goals: 102, team: 'Inter'},
  {id: 505, player: 'Maradonna', goals: 96, team: 'Neapoli'}
];

const PlayersContainer = () => {
  const [players, setPlayers] = useState(initial);

  useEffect(() => {
    getBestScorers().then(players => {
      setPlayers(players);
    })
  });

  return (
    <div className="players-container">
      <PlayersList players={players} />
    </div>);
};

export default PlayersContainer;
