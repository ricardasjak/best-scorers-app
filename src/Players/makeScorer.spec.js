import assert from 'assert';
import makeScorer from './makeScorer';

describe('makeScorer', () => {
  it('should make', () => {
    const player = {id: 11, name: 'Cristiano Ronaldo'};
    const teams = [{
      id: 10,
      name: 'Juventus F.C.',
      players: [{id: 11, name: 'Cristiano Ronaldo'}]
    }];
    const matches =
      [{
        'id': 22,
        'teamA': 10,
        'teamB': 5,
        'goals': [{'playerId': 11}, {'playerId': 11}, {'playerId': 12}, {'playerId': 6}]
      }];

    const expected = {
      ...player,
      goals: 2,
      team: 'Juventus F.C.'
    };

    assert.deepStrictEqual(makeScorer(player, matches, teams), expected);
  });
});

