import makeScorer from './makeScorer';

const urlPlayers =  "http://51.254.102.101:558/players";
const urlTeams =  "http://51.254.102.101:558/teams";
const urlMatches ="http://51.254.102.101:558/matches";

const getPlayers = () => {
  return fetch(urlPlayers).then(resp => resp.json());
};

const getTeams = () => {
  return fetch(urlTeams).then(resp => resp.json());
};

const getMatches = () => {
  return fetch(urlMatches).then(resp => resp.json());
};

export const getBestScorers = () => {
  return Promise.all([
    getPlayers(),
    getTeams(),
    getMatches()
  ]).then(([
    players,
    teams,
    matches
  ]) => {
    return Promise.resolve(players.map(player => makeScorer(player, matches, teams)));
  });
};
