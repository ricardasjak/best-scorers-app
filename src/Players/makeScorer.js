const makeScorer = (player, matches, teams) => {
  const {id, name} = player;

  const allGoals = matches.reduce((accum, {goals}) => [...accum, ...goals],
    []);

  const team = teams.find(({players}) => Boolean(players.find(teamPlayer => teamPlayer.id === id)));

  const goals = allGoals.filter(({playerId}) => playerId === id).length;

  return {
    ...player,
    team: (team || {}).name,
    goals
  };
};

export default makeScorer;
