import React from 'react';

const PlayersList = ({players}) => {
  return (
    <ul>
      {players.map(({id, name, goals, team}) => {
        return (
          <li className="row" key={id}>
            <div className="column">{name}</div>
            <div className="column">{team}</div>
            <div className="column">{goals}</div>
          </li>
        );
      })}
    </ul>);
};

export default PlayersList;
