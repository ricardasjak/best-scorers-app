import React from 'react';
import './App.css';
import PlayersContainer from "./Players/PlayersContainer";

function App() {
    return (
        <div className="App">
            <PlayersContainer/>
        </div>
    );
}

export default App;
